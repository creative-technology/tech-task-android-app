package us.ctp.techtask.ui.base;


/**
 * Base interface that any class that wants to act as a View in the MVP (Model View Presenter)
 * pattern must implement. Generally this interface will be extended by a more specific interface
 * that then usually will be implemented by an Activity or Fragment.
 *
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public interface MvpView {

}
