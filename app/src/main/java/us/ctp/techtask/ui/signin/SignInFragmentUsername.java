package us.ctp.techtask.ui.signin;

/**
 * @author Kirchhoff-
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.OnClick;
import us.ctp.techtask.R;
import us.ctp.techtask.ui.base.BaseFragment;
import us.ctp.techtask.util.InputErrorTextWatcher;


/**
 * @author Kirchhoff-
 */

public final class SignInFragmentUsername extends BaseFragment {

    @BindView(R.id.ilLogin)
    TextInputLayout ilLogin;
    @BindView(R.id.edLogin)
    TextInputEditText edLogin;
    @BindView(R.id.bContinue)
    Button bContinue;
    private LoginFragmentUsernameInteraction listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (LoginFragmentUsernameInteraction) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in_username, container, false);
        bindView(view);

        edLogin.addTextChangedListener(new InputErrorTextWatcher(ilLogin));
        return view;
    }

    @OnClick(R.id.bContinue)
    public void onContinueClick() {
        listener.onContinueClick(edLogin.getText().toString());
    }

    public void showEmptyUsernameError() {
        ilLogin.setErrorEnabled(true);
        ilLogin.setError(getString(R.string.username_empty_error));
    }

    public interface LoginFragmentUsernameInteraction {

        void onContinueClick(String userName);
    }
}
