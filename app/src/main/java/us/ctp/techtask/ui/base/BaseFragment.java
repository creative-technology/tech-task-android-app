package us.ctp.techtask.ui.base;

import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.at.tsvetkov.util.Log;
import us.ctp.techtask.ui.main.MainActivity;

/**
 * Base Fragment for bind/unbind ButterKnife and presenter
 * Created by Alexandr Tsvetkov on 09.11.2017.
 *
 * @param <T>
 */
public class BaseFragment<T extends Presenter> extends Fragment implements MvpView {

   protected T presenter;
   private Unbinder unbinder;

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         case android.R.id.home:
            getActivity().onBackPressed();
            return true;
         default:
            return super.onOptionsItemSelected(item);
      }
   }

   @Override
   public void onDestroyView() {
      super.onDestroyView();
      if (presenter != null) {
         presenter.detachView();
      }
   }

   @Override
   public void onDestroy() {
      unbinder.unbind();
      super.onDestroy();
   }

   /**
    * Bind view created in onCreateView method to ButterKnife and attach to presenter
    *
    * @param rootView
    */
   protected void bindView(View rootView) {
      if (rootView == null) {
         Log.e("Unfortunately rootView is null or destroyed", new NullPointerException());
      }
      unbinder = ButterKnife.bind(this, rootView);
      presenter = createPresenter();
      if (presenter != null) {
         presenter.attachView(this);
      }
   }

   public MainActivity getMainActivity() {
      return (MainActivity) getActivity();
   }

   protected void setToolbarTitle(int id) {
      getMainActivity().getSupportActionBar().setTitle(id);
   }

   protected void setToolbarTitle(String title) {
      getMainActivity().getSupportActionBar().setTitle(title);
   }

   protected void showToolbar(boolean isShown) {
      if (isShown) {
         getMainActivity().getSupportActionBar().show();
      } else {
         getMainActivity().getSupportActionBar().hide();
      }
   }

   protected void setDisplayHomeAsUpEnabled(boolean isShow) {
      getMainActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(isShow);
      getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(isShow);
   }

   private T createPresenter() {
      try {
         Type type = getClass().getGenericSuperclass();
         ParameterizedType paramType;
         try {
            paramType = (ParameterizedType) type;
         } catch (ClassCastException e) {
            Log.i(getClass().toString() + " is not parameterized");
            return null;
         }
         Class<T> clazz = (Class<T>) paramType.getActualTypeArguments()[0];
         return clazz.newInstance();
      } catch (java.lang.InstantiationException e) {
         Log.e(e.toString());
      } catch (IllegalAccessException e) {
         Log.e(e.toString());
      }
      return null;
   }

}

