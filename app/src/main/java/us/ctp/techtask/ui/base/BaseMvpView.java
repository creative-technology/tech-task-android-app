package us.ctp.techtask.ui.base;

import us.ctp.techtask.data.remote.AbstractParser;

/**
 * Base interface
 * <p>
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public interface BaseMvpView extends MvpView {

   void onGetDataFailed(String message);

   void showData(AbstractParser dataObj);

}
