package us.ctp.techtask.ui.coursesection;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import us.ctp.techtask.R;

/**
 * Created by Angel on 10.11.2017.
 */

public class CourseViewHolder extends RecyclerView.ViewHolder{

    public ImageView imgThumbnail;
    public TextView txtSubject;
    public TextView txtName;

    public CourseViewHolder(View itemView) {
        super(itemView);

        imgThumbnail = (ImageView) itemView.findViewById(R.id.course_list_thumbnail);
        txtSubject = (TextView) itemView.findViewById(R.id.course_list_subject);
        txtName = (TextView) itemView.findViewById(R.id.course_list_name);
    }
}
