package us.ctp.techtask.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import us.ctp.techtask.R;
import us.ctp.techtask.ui.base.BaseFragment;
import us.ctp.techtask.ui.main.MainActivity;
import us.ctp.techtask.util.Warning;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class SignInFragment extends BaseFragment<SignInPresenter> implements SignInMvpView {

    private final static String USERNAME_FRAGMENT_TAG = "USERNAME_FRAGMENT_TAG";
    private final static String USERNAME_PASSWORD_TAG = "USERNAME_PASSWORD_TAG";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer,
                        new SignInFragmentUsername(), USERNAME_FRAGMENT_TAG).commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
        bindView(rootView);
        return rootView;
    }

    @Override
    public void onSuccessLogin() {
       MainActivity.start(this.getContext());
    }

    @Override
    public void showLoginError(String errorMessage) {
        Warning.showError(getActivity(), errorMessage);
    }

    @Override
    public void moveToEnterPassword(@NonNull String userName) {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer,
                        SignInFragmentPassword.getInstance(userName), USERNAME_PASSWORD_TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showEmptyUsernameError() {
        SignInFragmentUsername loginFragmentUsername =
                (SignInFragmentUsername) getChildFragmentManager()
                        .findFragmentByTag(USERNAME_FRAGMENT_TAG);
        loginFragmentUsername.showEmptyUsernameError();
    }

    @Override
    public void showEmptyPasswordError() {
        SignInFragmentPassword loginFragmentUsername =
                (SignInFragmentPassword) getChildFragmentManager()
                        .findFragmentByTag(USERNAME_PASSWORD_TAG);
        loginFragmentUsername.showEmptyPasswordError();
    }

    public void auth(@Nullable String login, @Nullable String password) {
        presenter.sendSignInRequest(getActivity(), login, password);
    }

}
