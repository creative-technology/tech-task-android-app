package us.ctp.techtask.ui.signin;

import android.app.Activity;


import us.ctp.techtask.data.remote.ErrorMessageCreator;
import us.ctp.techtask.data.remote.Rest;
import us.ctp.techtask.ui.base.BasePresenter;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class SignInPresenter extends BasePresenter<SignInMvpView> {

   public void sendSignInRequest(Activity activity, String email, String password) {
      Rest.signIn(activity, email, password, (obj, statusCode, message) -> {
         if (obj != null && obj.isSuccess()) {
            if (isViewAttached()) {
               getMvpView().onSuccessLogin();
            }
         } else {
            if (isViewAttached()) {
                getMvpView().showLoginError(ErrorMessageCreator.create(obj, statusCode, message));
            }
         }
      });
   }
}
