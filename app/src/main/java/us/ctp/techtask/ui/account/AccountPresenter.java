package us.ctp.techtask.ui.account;

import android.app.Activity;

import us.ctp.techtask.data.remote.ErrorMessageCreator;
import us.ctp.techtask.data.remote.Rest;
import us.ctp.techtask.ui.base.BasePresenter;

/**
 * Created by Angel on 10.11.2017.
 */

public class AccountPresenter extends BasePresenter<AccountMvpView>{

    public void getUserData(Activity activity) {
        Rest.getUser(activity,  (obj, statusCode, message) -> {
            if (obj != null && obj.isSuccess()) {
                if (isViewAttached()) {
                    getMvpView().onGotUserData(obj.user);
                }
            } else {
                if (isViewAttached()) {
                    getMvpView().showError(ErrorMessageCreator.create(obj, statusCode, message));
                }
            }
        });
    }

    public void logoutUser(Activity activity) {
        Rest.logout(activity,  (obj, statusCode, message) -> {
            if (statusCode == 200) {
                if (isViewAttached()) {
                    getMvpView().onSuccessLogout();
                }
            } else {
                if (isViewAttached()) {
                    getMvpView().showError(ErrorMessageCreator.create(obj, statusCode, message));
                }
            }
        });
    }
}
