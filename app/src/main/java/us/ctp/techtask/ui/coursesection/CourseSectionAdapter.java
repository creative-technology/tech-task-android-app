package us.ctp.techtask.ui.coursesection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import us.ctp.techtask.R;
import us.ctp.techtask.data.models.CourseItem;

/**
 * Created by Angel on 10.11.2017.
 */

public class CourseSectionAdapter extends RecyclerView.Adapter<CourseViewHolder>{

    private List<CourseItem> courseList;
    private Context context;

    CourseSectionAdapter(Context context, List<CourseItem> courseList) {
        this.courseList = courseList;
        this.context = context;
    }

    @Override
    public CourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_card_view, parent, false);
        return new CourseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CourseViewHolder holder, int position) {
        holder.txtName.setText(courseList.get(position).getName());
        holder.txtSubject.setText(courseList.get(position).getSubject());
        Picasso.with(context).load(courseList.get(position).getThumbnail()).into(holder.imgThumbnail);
    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
