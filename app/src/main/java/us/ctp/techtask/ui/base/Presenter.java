package us.ctp.techtask.ui.base;

/**
 * Every presenter in the app must either implement this interface or extend BasePresenter
 * indicating the MvpView type that wants to be attached with.
 * <p>
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public interface Presenter<V extends MvpView> {

   void attachView(V mvpView);

   void detachView();

}
