package us.ctp.techtask.ui.coursesection;


import us.ctp.techtask.data.models.User;
import us.ctp.techtask.data.remote.parsers.CourseSectionParser;
import us.ctp.techtask.ui.base.MvpView;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public interface CourseSectionMvpView extends MvpView {

   void onGotCourseSectionData(CourseSectionParser dataObj);

   void showError(String errorMessage);

}
