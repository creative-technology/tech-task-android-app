package us.ctp.techtask.ui.coursesection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import us.ctp.techtask.R;
import us.ctp.techtask.data.models.CourceSection;
import us.ctp.techtask.data.remote.parsers.CourseSectionParser;
import us.ctp.techtask.ui.base.BaseFragment;
import us.ctp.techtask.util.Warning;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */


public class CourseSectionFragment extends BaseFragment<CourseSectionPresenter> implements CourseSectionMvpView {

   @BindView(R.id.course_section_recycler_view)
   RecyclerView courseListView;

   private CourseSectionAdapter adapter;

   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View rootView = inflater.inflate(R.layout.fragment_course_section, container, false);
      bindView(rootView);
      showToolbar(true);
      setDisplayHomeAsUpEnabled(false);
      setToolbarTitle(R.string.title_courses);

      // presenter.getCourseSectionData(getMainActivity(), "57c5a41b1163cc1b00ff1096");
      // because we can't get data from the server we will use temporary mock result

      adapter = new CourseSectionAdapter(getContext(), CourceSection.createCoursesFromMock());
      courseListView.setLayoutManager(new LinearLayoutManager(getContext()));
      courseListView.setAdapter(adapter);

      return rootView;
   }

   @Override
   public void onGotCourseSectionData(CourseSectionParser obj) {
      Warning.todo(this, "show CourseSection data");
   }

   @Override
   public void showError(String errorMessage) {
      Warning.showError(getActivity(), errorMessage);
   }

}
