package us.ctp.techtask.ui.signin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import us.ctp.techtask.R;
import us.ctp.techtask.ui.base.BaseActivity;
import us.ctp.techtask.util.TextViewLinkHandler;

/**
 * @author Kirchhoff-
 */

public class SignInActivity extends BaseActivity implements SignInFragmentUsername.LoginFragmentUsernameInteraction,
        SignInFragmentPassword.LoginFragmentPasswordInteraction {

    private final static String SIGN_IN_FRAGMENT = "SIGN_IN_FRAGMENT";

    @BindView(R.id.tvService)
    TextView tvService;

    private String login;

    public static void start(Context context) {
        Intent intent = new Intent(context, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.frameLayout, new SignInFragment(), SIGN_IN_FRAGMENT)
                .commit();

        tvService.setMovementMethod(new TextViewLinkHandler() {
            @Override
            public void onLinkClick(String url) {
                showTermsOfService();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment signInFragment = getSupportFragmentManager().findFragmentByTag(SIGN_IN_FRAGMENT);
        if (signInFragment.getChildFragmentManager().getBackStackEntryCount() > 0)
            signInFragment.getChildFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }

    private void showTermsOfService() {
        Toast.makeText(this, R.string.terms_of_service_text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onContinueClick(String userName) {
        SignInFragment fragment = (SignInFragment) getSupportFragmentManager()
                .findFragmentByTag(SIGN_IN_FRAGMENT);
        if (userName == null || userName.isEmpty()) {
            fragment.showEmptyUsernameError();
        } else {
            this.login = userName;
            fragment.moveToEnterPassword(login);
        }
    }

    @Override
    public void onSignInClick(String password) {
        SignInFragment fragment = (SignInFragment) getSupportFragmentManager()
                .findFragmentByTag(SIGN_IN_FRAGMENT);
        if (password == null || password.isEmpty())
            fragment.showEmptyPasswordError();
        else
            fragment.auth(login, password);
    }
}
