package us.ctp.techtask.ui.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import ua.at.tsvetkov.application.AppConfig;
import us.ctp.techtask.R;
import us.ctp.techtask.data.models.User;
import us.ctp.techtask.ui.base.BaseFragment;
import us.ctp.techtask.ui.signin.SignInActivity;
import us.ctp.techtask.util.Warning;

/**
 * Created by Angel on 10.11.2017.
 */

public class AccountFragment extends BaseFragment<AccountPresenter> implements AccountMvpView {

    @BindView(R.id.account_section_email)
    TextView txtEmail;

    @BindView(R.id.account_section_user_name)
    TextView txtName;

    @BindView(R.id.account_section_profile_image)
    TextView txtProfileImage;

    @BindView(R.id.account_section_logout)
    Button btnLogout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account_section, container, false);
        bindView(rootView);
        showToolbar(true);
        setDisplayHomeAsUpEnabled(false);
        setToolbarTitle(R.string.title_account);

        presenter.getUserData(getActivity());

        return rootView;
    }

    @OnClick(R.id.account_section_logout)
    public void logoutClick() {
         presenter.logoutUser(getActivity());
    }

    @Override
    public void onSuccessLogout() {
        AppConfig.clear();
        AppConfig.save();
        SignInActivity.start(getActivity());
    }

    @Override
    public void onGotUserData(User user) {
        User model = User.getUserModel();
        if(model != null) {
            txtName.setText(model.getName());
            txtEmail.setText(model.getEmail());

            StringBuilder strBuilder = new StringBuilder();

            if(!TextUtils.isEmpty(model.getFirstName()))
                strBuilder.append(model.getFirstName().charAt(0));

            if(!TextUtils.isEmpty(model.getLastName()))
                strBuilder.append(model.getLastName().charAt(0));

            txtProfileImage.setText(strBuilder.toString());
        }
    }

    @Override
    public void showError(String errorMessage) {
        Warning.showError(getActivity(), "Logout error");
    }
}
