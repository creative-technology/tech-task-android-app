package us.ctp.techtask.ui.signin;


import android.support.annotation.NonNull;

import us.ctp.techtask.ui.base.MvpView;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public interface SignInMvpView extends MvpView {

   void onSuccessLogin();

   void showLoginError(String errorMessage);

   void moveToEnterPassword(@NonNull String userName);

   void showEmptyUsernameError();

   void showEmptyPasswordError();


}
