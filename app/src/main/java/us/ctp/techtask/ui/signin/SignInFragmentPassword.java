package us.ctp.techtask.ui.signin;

/**
 * @author Kirchhoff-
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import us.ctp.techtask.R;
import us.ctp.techtask.ui.base.BaseFragment;
import us.ctp.techtask.util.InputErrorTextWatcher;


/**
 * @author Kirchhoff-
 */

public class SignInFragmentPassword extends BaseFragment {

    private final static String USERNAME_ARG = "USERNAME_ARG";
    @BindView(R.id.ilPassword)
    TextInputLayout ilPassword;
    @BindView(R.id.edPassword)
    TextInputEditText edPassword;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    private LoginFragmentPasswordInteraction listener;

    public static SignInFragmentPassword getInstance(@NonNull String userName) {
        SignInFragmentPassword loginFragmentPassword = new SignInFragmentPassword();
        Bundle args = new Bundle();
        args.putString(USERNAME_ARG, userName);
        loginFragmentPassword.setArguments(args);
        return loginFragmentPassword;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (LoginFragmentPasswordInteraction) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in_password, container, false);
        bindView(view);
        edPassword.addTextChangedListener(new InputErrorTextWatcher(ilPassword));
        if (getArguments() != null && getArguments().containsKey(USERNAME_ARG))
            tvUserName.setText(getArguments().getString(USERNAME_ARG));

        return view;
    }

    @OnClick(R.id.bSignIn)
    public void singInClick() {
        listener.onSignInClick(edPassword.getText().toString());
    }

    @OnClick(R.id.llBack)
    public void backClick() {
        if (getActivity() != null)
            getActivity().onBackPressed();
    }

    public void showEmptyPasswordError() {
        ilPassword.setErrorEnabled(true);
        ilPassword.setError(getString(R.string.password_empty_error));
    }

    public interface LoginFragmentPasswordInteraction {

        void onSignInClick(String password);
    }
}
