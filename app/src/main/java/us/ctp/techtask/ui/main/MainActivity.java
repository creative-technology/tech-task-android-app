package us.ctp.techtask.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import us.ctp.techtask.R;
import us.ctp.techtask.ui.base.BaseActivity;
import us.ctp.techtask.ui.account.AccountFragment;
import us.ctp.techtask.ui.coursesection.CourseSectionFragment;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class MainActivity extends BaseActivity {

   @BindView(R.id.mainActivityNavigationBar)
   BottomNavigationView navigation;

   private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
      switch (item.getItemId()) {
         case R.id.navigation_courses:
            setContainerFragmentNoBackStack(CourseSectionFragment.class);
            return true;
         case R.id.navigation_account:
            setContainerFragmentNoBackStack(AccountFragment.class);
            return true;
      }
      return false;
   };

   public static void start(Context context) {
      Intent intent = new Intent(context, MainActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      context.startActivity(intent);
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      setContentView(R.layout.activity_main);
      setContainerId(R.id.mainActivityContentFrame);
      ButterKnife.bind(this);

      setContainerFragmentNoBackStack(CourseSectionFragment.class);

      navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
   }

}
