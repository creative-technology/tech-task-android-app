package us.ctp.techtask.ui.account;

import us.ctp.techtask.data.models.User;
import us.ctp.techtask.ui.base.MvpView;

/**
 * Created by Angel on 10.11.2017.
 */

public interface AccountMvpView extends MvpView {

    void onSuccessLogout();

    void onGotUserData(User dataObj);

    void showError(String errorMessage);
}
