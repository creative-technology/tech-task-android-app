package us.ctp.techtask.ui.coursesection;

import android.app.Activity;

import us.ctp.techtask.data.remote.ErrorMessageCreator;
import us.ctp.techtask.data.remote.Rest;
import us.ctp.techtask.ui.base.BasePresenter;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class CourseSectionPresenter extends BasePresenter<CourseSectionMvpView> {

   public void getCourseSectionData(Activity activity, String schoolId) {

      Rest.getCourseSection(activity,  schoolId, (obj, statusCode, message) -> {
         if (obj != null && obj.isSuccess()) {
            if (isViewAttached()) {
               getMvpView().onGotCourseSectionData(obj);
            }
         } else {
            if (isViewAttached()) {
               getMvpView().showError(ErrorMessageCreator.create(obj, statusCode, message));
            }
         }
      });
   }

}
