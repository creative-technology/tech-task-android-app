package us.ctp.techtask.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import ua.at.tsvetkov.util.Log;
import us.ctp.techtask.BuildConfig;


/**
 * Abstract activity that every other Activity in this application must implement.
 * <p>
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public abstract class BaseActivity extends AppCompatActivity {

   private int containerViewId;
   private FragmentManager.FragmentLifecycleCallbacks fragmentLifecycleCallbacks;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      attachFragmentLifecycleLogger();
   }

   @Override
   protected void onDestroy() {
      super.onDestroy();
      detachFragmentLifecycleLogger();
   }

   /**
    * Set content view for replace by fragments with setContainerFragment(...) or setContainerFragmentNoBackStack(...) methods
    *
    * @param containerViewId
    */
   public void setContainerId(int containerViewId) {
      this.containerViewId = containerViewId;
   }

   //=================== add Fragment with back stack ====================

   public void setContainerFragment(Class<? extends Fragment> clazz) {
      setContainerFragment(clazz, clazz.getSimpleName(), null);
   }

   public void setContainerFragment(Class<? extends Fragment> clazz, Bundle bundle) {
      setContainerFragment(clazz, clazz.getSimpleName(), bundle);
   }

   public void setContainerFragment(Class<? extends Fragment> clazz, String tag, Bundle bundle) {
      setContainerFragment(takeFragment(clazz, bundle), tag);
   }

   public void setContainerFragment(Fragment fragment, String tag) {
      getSupportFragmentManager()
              .beginTransaction()
              .replace(containerViewId, fragment, tag)
              .addToBackStack(tag)
              .commit();
   }

   //=================== add Fragment with NO back stack ====================

   public void setContainerFragmentNoBackStack(Class<? extends Fragment> clazz) {
      setContainerFragmentNoBackStack(clazz, clazz.getSimpleName(), null);
   }

   public void setContainerFragmentNoBackStack(Class<? extends Fragment> clazz, Bundle bundle) {
      setContainerFragmentNoBackStack(clazz, clazz.getSimpleName(), bundle);
   }

   public void setContainerFragmentNoBackStack(Class<? extends Fragment> clazz, String tag, Bundle bundle) {
      Fragment fragment = takeFragment(clazz, bundle);
      getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
      getSupportFragmentManager()
              .beginTransaction()
              .replace(containerViewId, fragment, tag)
              .commit();

   }

   @Nullable
   private Fragment takeFragment(Class<? extends Fragment> clazz, Bundle bundle) {
      Fragment fragment = null;
      try {
         fragment = clazz.newInstance();
         if (bundle == null) {
            bundle = new Bundle();
         }
         fragment.setArguments(bundle);
      } catch (InstantiationException | IllegalAccessException e) {
         Log.e(e);
      }
      return fragment;
   }

   private void attachFragmentLifecycleLogger() {
      if (BuildConfig.DEBUG) {
         fragmentLifecycleCallbacks = createFragmentLifecycleCallbacks();
         getSupportFragmentManager().registerFragmentLifecycleCallbacks(fragmentLifecycleCallbacks, true);
      }
   }

   private void detachFragmentLifecycleLogger() {
      if (BuildConfig.DEBUG) {
         getSupportFragmentManager().unregisterFragmentLifecycleCallbacks(fragmentLifecycleCallbacks);
      }
   }

   private FragmentManager.FragmentLifecycleCallbacks createFragmentLifecycleCallbacks() {
      return new FragmentManager.FragmentLifecycleCallbacks() {

         @Override
         public void onFragmentAttached(FragmentManager fm, Fragment f, Context context) {
            super.onFragmentAttached(fm, f, context);
            int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
            Log.d("FRAGMENT STACK [" + backStackCount + "] attached " + f.getClass().getSimpleName());
            printFragmentsStack(backStackCount);
         }

         @Override
         public void onFragmentDetached(FragmentManager fm, Fragment f) {
            super.onFragmentDetached(fm, f);
            int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
            Log.d("FRAGMENT STACK [" + backStackCount + "] detached " + f.getClass().getSimpleName());
            printFragmentsStack(backStackCount);
         }

      };
   }

   private void printFragmentsStack(int backStackCount) {
      for (int i = 0; i < backStackCount; i++) {
         FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(i);
         Log.d("     #" + i + "  " + entry.getName());
      }
   }

}
