package us.ctp.techtask.ui.splash;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import ua.at.tsvetkov.application.AppConfig;
import us.ctp.techtask.R;
import us.ctp.techtask.ui.main.MainActivity;
import us.ctp.techtask.ui.signin.SignInActivity;
import us.ctp.techtask.util.Key;

/**
 * @author Kirchhoff-
 */

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        if (AppConfig.getBoolean(Key.IS_USER_LOGIN, false)) {
//            //TODO Here should be starting of Activity with classes
//            MainActivity.start(this);
//        } else {
//            SignInActivity.start(this);
//        }
        if (!TextUtils.isEmpty(AppConfig.getString(Key.ACCESS_TOKEN, null))) {
            //TODO Here should be starting of Activity with classes
            MainActivity.start(this);
        } else {
            SignInActivity.start(this);
        }
    }
}
