package us.ctp.techtask.util;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class Key {

   public static final String ACCESS_TOKEN = "ACCESS_TOKEN";

   private Key() {
      // nothing
   }

}
