package us.ctp.techtask.util;


import us.ctp.techtask.BuildConfig;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class Constant {


   private Constant(){
      // nothing to do
   }

   public static final String GIT_STAMP = BuildConfig.GIT_SHA;
   public static final boolean IS_DEBUG = BuildConfig.DEBUG;
   public static String SERVER = BuildConfig.END_POINT;

   public static final String DIR_TMP = "tmp";
   public static final int TIMEOUT = 10000;

}
