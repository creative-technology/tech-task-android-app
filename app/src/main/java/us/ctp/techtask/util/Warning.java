package us.ctp.techtask.util;

import android.app.Activity;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;

import ua.at.tsvetkov.util.Log;
import us.ctp.techtask.R;
import us.ctp.techtask.TechTaskApplication;

/**
 * Show warning messages.
 * <p>
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class Warning {

   private static boolean isSnackBar = false;
   private static int errorColor = Color.RED;
   private static int okColor = TechTaskApplication.getInstance().getResources().getColor(R.color.colorPrimary);

   private Warning() {
      // empty
   }

   public static void setSnackBarDefault() {
      isSnackBar = true;
   }

   public static void setDialogDefault() {
      isSnackBar = false;
   }

   public static void setErrorColor(int errorColor) {
      Warning.errorColor = errorColor;
   }

   public static void setOkColor(int okColor) {
      Warning.okColor = okColor;
   }

   /**
    * Stub for not implemented code.
    */
   public static void todo(Object obj, String todo) {
      Log.w(obj, "TODO: " + todo);
   }

   /**
    * Shows error
    *
    * @param activity
    * @param text
    */
   public static void showError(final Activity activity, final String text) {
      showWarning(activity, true, text);
   }

   /**
    * Shows error
    *
    * @param activity
    * @param stringId
    */
   public static void showError(final Activity activity, final int stringId) {
      showWarning(activity, true, activity.getString(stringId));
   }

   /**
    * Shows warning
    *
    * @param activity
    * @param text
    */
   public static void showOk(final Activity activity, final String text) {
      showWarning(activity, false, text);
   }

   /**
    * Shows warning
    *
    * @param activity
    * @param stringId
    */
   public static void showOk(final Activity activity, final int stringId) {
      showWarning(activity, false, activity.getString(stringId));
   }

   /**
    * Stub for not implemented code. Show this in the log and warning dialog for user.
    *
    * @param activity
    * @param obj      - not implemented object ("this" is acceptable)
    */
   public static void notImplemented(Activity activity, Object obj) {
      Log.w(obj, "TODO: This method is not implemented yet!");
      showError(activity, "Is not implemented yet");
   }

   // ================ Private methods =================

   private static void showWarning(Activity activity, boolean isError, String message) {
      if (activity.getMainLooper().getThread() == Thread.currentThread()) {
         createWarning(activity, isError, message);
      } else {
         activity.runOnUiThread(() -> createWarning(activity, isError, message));
      }
   }

   private static void createWarning(Activity activity, boolean isError, String message) {
      if (isSnackBar) {
         Snackbar sb = Snackbar.make(activity.getCurrentFocus(), message, Snackbar.LENGTH_LONG);
         if (isError) {
            sb.setActionTextColor(errorColor);
         } else {
            sb.setActionTextColor(okColor);
         }
         sb.show();
      } else {
         AlertDialog.Builder builder = new AlertDialog.Builder(activity);
         builder.setMessage(message);
         if (isError) {
            builder.setTitle(android.R.string.dialog_alert_title);
         }
         builder.setPositiveButton(android.R.string.ok, (dialog, id) -> dialog.dismiss());
         builder.create().show();
      }
   }

}
