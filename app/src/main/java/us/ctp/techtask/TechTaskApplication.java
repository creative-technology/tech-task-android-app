package us.ctp.techtask;

import android.app.Application;
import android.app.FragmentManager;

import ua.at.tsvetkov.application.AppConfig;
import ua.at.tsvetkov.data_processor.DataProcessor;
import ua.at.tsvetkov.data_processor.DataProcessorConfiguration;
import ua.at.tsvetkov.data_processor.helpers.Scheme;
import ua.at.tsvetkov.util.Log;
import us.ctp.techtask.util.Constant;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class TechTaskApplication extends Application {

   private static TechTaskApplication sInstance;

   @Override
   public void onCreate() {
      super.onCreate();
      sInstance = this;
      
      AppConfig.init(this);
      AppConfig.printInfo(this);

      Log.setStamp(Constant.GIT_STAMP);
      Log.enableActivityLifecycleAutoLogger(this);
      FragmentManager.enableDebugLogging(Constant.IS_DEBUG);

      DataProcessorConfiguration config = DataProcessorConfiguration.getBuilder()
              .setHost(Constant.SERVER)
              .setScheme(Scheme.HTTP.toString())
              .setHttpUserAgent(System.getProperty("http.agent"))
              .setTestServerUrl(Constant.SERVER)
              .setTimeout(Constant.TIMEOUT)
              .setLogEnabled(Constant.IS_DEBUG)
              .build();
      DataProcessor.getInstance().init(config);

   }

   public static TechTaskApplication getInstance() {
      return sInstance;
   }

}
