package us.ctp.techtask.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ua.at.tsvetkov.util.Log;

/**
 * Created by lordtao on 09.11.2017.
 */

public class BodyCreator {

    public static String createSignInBody(String username, String password) {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("username", username);
            jsonObj.put("password", password);
            return jsonObj.toString();
        } catch (JSONException e) {
            Log.e(e);
            return "";
        }
    }

}
