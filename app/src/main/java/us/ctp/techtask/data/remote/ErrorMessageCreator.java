package us.ctp.techtask.data.remote;

import android.text.TextUtils;

import java.net.HttpURLConnection;

import ua.at.tsvetkov.data_processor.helpers.ConnectionConstants;
import ua.at.tsvetkov.data_processor.templates.JSONDataAbstract;
import us.ctp.techtask.R;
import us.ctp.techtask.TechTaskApplication;

public final class ErrorMessageCreator {

   /**
    * Create detail error message for server responce data
    *
    * @param obj
    * @param statusCode
    * @param responseMessage
    * @return
    */
   public static String create(JSONDataAbstract obj, int statusCode, String responseMessage) {
      if (statusCode == ConnectionConstants.NO_INTERNET_CONNECTION) {
         return TechTaskApplication.getInstance().getString(R.string.error_no_internet);
      }
      if (statusCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
         return TechTaskApplication.getInstance().getString(R.string.error_internal_server);
      }
      if (obj == null) {
         return responseMessage;
      }
      if (!TextUtils.isEmpty(obj.getMessage())) {
         return obj.getMessage();
      } else {
         if (obj != null && obj.getMessage() != null && obj.getMessage().length() > 0) {
            return "HTTP Error:" + statusCode + " " + obj.getMessage();
         } else {
            return "HTTP Error:" + statusCode + " " + responseMessage;
         }
      }
   }

}
