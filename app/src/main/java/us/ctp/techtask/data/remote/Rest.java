package us.ctp.techtask.data.remote;

import android.content.Context;

import ua.at.tsvetkov.application.AppConfig;
import ua.at.tsvetkov.data_processor.DataProcessor;
import ua.at.tsvetkov.data_processor.processors.Processor;
import ua.at.tsvetkov.data_processor.requests.GetRequest;
import ua.at.tsvetkov.data_processor.requests.PostRequest;
import ua.at.tsvetkov.data_processor.requests.Request;
import us.ctp.techtask.data.remote.parsers.CourseSectionParser;
import us.ctp.techtask.data.remote.parsers.LogoutParser;
import us.ctp.techtask.data.remote.parsers.SignInParser;
import us.ctp.techtask.data.remote.parsers.UserParser;
import us.ctp.techtask.util.Key;

/**
 * Created by Alexandr Tsvetkov on 09.11.2017.
 */
public class Rest {

   private static final String PROPERTY_CONTENT_TYPE = "Content-Type";
   private static final String PROPERTY_ACCEPT = "accept-language";
   private static final String PROPERTY_DEVICE_ID = "deviceid";
   private static final String PROPERTY_APP_VERSION = "appVersion";
   private static final String PROPERTY_AUTHORIZATION = "Authorization";
   private static final String APPLICATION_JSON = "application/json";
   private static final String BEARER = "Bearer ";
   private static final String PROGRESS_TITLE = "Please wait.";
   private static final String PROGRESS_MESSAGE = "Connecting to server...";
   // URL paths
   private static final String USER = "/user";
   private static final String AUTHENTICATE_USER = USER + "/authenticate";
   private static final String COURSE_SECTION = "/coursesection";
   private static final String LOGOUT_USER = USER + "/logout";
   // Requests params
   private static final String PARAM_SCHOOL_ID = "schoolId";
   private static String appVersion = "v1";
   private static String deviceid = "b5288239-b29b-4f4a-b231-618cdd6c776e";
   private static String accessToken = "";

   static {
      accessToken = AppConfig.getString(Key.ACCESS_TOKEN);
   }

   public static void setAccessToken(String token) {
      accessToken = token;
   }

   /**
    * Sign in app
    *
    * @param context
    * @param username
    * @param password
    * @param callback
    */
   public static void signIn(Context context, String username, String password, Processor.Callback<SignInParser> callback) {
      final Request request = PostRequest
              .newInstance()
              .setPath(AUTHENTICATE_USER)
              .addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE)
              .addRequestProperty(PROPERTY_CONTENT_TYPE, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_ACCEPT, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_DEVICE_ID, deviceid)
              .addRequestProperty(PROPERTY_APP_VERSION, appVersion)
              .addBody(BodyCreator.createSignInBody(username, password))
              .build();
      DataProcessor.getInstance().executeAsync(request, SignInParser.class, callback);
   }

   /**
    * @param context
    * @param schoolId
    * @param callback
    */
   public static void getCourseSection(Context context, String schoolId, Processor.Callback<CourseSectionParser> callback) {
      Request request = GetRequest
              .newInstance()
              .setPath(COURSE_SECTION)
              .addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE)
              .addRequestProperty(PROPERTY_CONTENT_TYPE, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_ACCEPT, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_AUTHORIZATION, BEARER + accessToken)
              .addRequestProperty(PROPERTY_DEVICE_ID, deviceid)
              .addRequestProperty(PROPERTY_APP_VERSION, appVersion)
              .addGetParam(PARAM_SCHOOL_ID, schoolId)
              .build();
      if (context != null) {
         request = request.addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE);
      }
      DataProcessor.getInstance().executeAsync(request.build(), CourseSectionParser.class, callback);
   }

   public static void getUser(Context context, Processor.Callback<UserParser> callback) {
      Request request = GetRequest
              .newInstance()
              .setPath(USER)
              .addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE)
              .addRequestProperty(PROPERTY_CONTENT_TYPE, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_ACCEPT, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_AUTHORIZATION, BEARER + accessToken)
              .addRequestProperty(PROPERTY_DEVICE_ID, deviceid)
              .addRequestProperty(PROPERTY_APP_VERSION, appVersion)
              .build();
      if (context != null) {
         request = request.addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE);
      }
      DataProcessor.getInstance().executeAsync(request.build(), UserParser.class, callback);
   }

   public static void logout(Context context, Processor.Callback<LogoutParser> callback) {
      Request request = GetRequest
              .newInstance()
              .setPath(LOGOUT_USER)
              .addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE)
              .addRequestProperty(PROPERTY_CONTENT_TYPE, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_ACCEPT, APPLICATION_JSON)
              .addRequestProperty(PROPERTY_AUTHORIZATION, BEARER + accessToken)
              .addRequestProperty(PROPERTY_DEVICE_ID, deviceid)
              .addRequestProperty(PROPERTY_APP_VERSION, appVersion)
              .build();

      if (context != null) {
         request = request.addProgressDialog(context, PROGRESS_TITLE, PROGRESS_MESSAGE);
      }

      DataProcessor.getInstance().executeAsync(request, LogoutParser.class, callback);
   }

}
