package us.ctp.techtask.data.models;

import android.text.TextUtils;

import com.google.gson.Gson;

import org.json.JSONObject;

import us.ctp.techtask.util.Warning;

/**
 * Created by lordtao on 09.11.2017.
 */

public class User {

    private String fName;
    private String lName;
    private String name;
    private String email;
    private String id;

    public String getFirstName() {
        return fName;
    }

    public String getLastName() {
        return lName;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    private static String json;

    public static User createFromJSON(JSONObject jsonObject) {
        User user = new User();
        Warning.todo(jsonObject, "Need to parse correct User object for your needs");
        json = jsonObject.toString();

        return user;
    }

    public static User getUserModel() {
        if(!TextUtils.isEmpty(json)) {
            Gson gson = new Gson();
            User userModel = (User) gson.fromJson(json.toString(), User.class);

            return userModel;
        } else {
            return null;
        }
    }

    // Example of JSON

//   {
//      "fName": "Dev",
//           "lName": "Demo",
//           "name": "Dev Demo",
//           "email": "devdemo@pulse.com",
//           "thumbnail": "",
//           "id": "5906a3f321af5431005883b0",
//           "uid": "devdemo",
//           "username": "devdemo",
//           "createdBy": "padmin",
//           "externalRef": [],
//      "schools": [
//      {
//         "id": "5742722bca57901e00d6da44",
//              "name": "Pearson School",
//              "saId": "572ca8519e20ef1e00878f87",
//              "countryCode": "",
//              "geoId": "572ca7f79e20ef1e00878f86",
//              "geoName": "Default",
//              "saName": "Default",
//              "roleValue": "student",
//              "moodleUserId": 37,
//              "createdBy": "padmin",
//              "externalRef": [],
//         "products": [
//         {
//            "endDate": "2017-11-10T05:00:00.000Z",
//                 "startDate": "2017-11-01T14:39:13.431Z",
//                 "productId": "58896cfb4f6b07700094cec5"
//         },
//         {
//            "endDate": "2017-11-10T05:00:00.000Z",
//                 "startDate": "2017-11-01T14:39:13.431Z",
//                 "productId": "58896d1944c10c6a0078cadb"
//         }
//      ],
//         "links": {
//         "moodle": "http://moodle-apache.pulse-prd.svc.cluster.local/moodle/8acd4ee20ae143cc862c3d9294696af6/",
//                 "services": "https://api.pulse.pearson.com",
//                 "appConfig": "https://s3.amazonaws.com/pulse-lms/config/pulse.app.config.R7.json",
//                 "moodleToken": "16579f88173449d2bbd359d5de80479e",
//                 "appConfigAPI": "http://caas.pearson-intl.com/appconfig/s3path?folder=pulse-lms/config&configfile=pulse.app.config.R7.json",
//                 "languagePackAPI": "http://caas.pearson-intl.com/appconfig/s3path?folder=pulse-lms/language_pack&configfile=pulse.default.language_pack.json",
//                 "heroImage": "https://s3.amazonaws.com/pulse-lms/content/school-hero-1.jpg",
//                 "textbooksThumbnail": "https://s3.amazonaws.com/pulse-lms/content/default_textbooks.jpg",
//                 "website": "https://www.pulse.pearson.com",
//                 "cookie": "https://s3.amazonaws.com/pulse-lms/legal/cookie.htm",
//                 "privacy": "https://za.pearson.com/Pulse-learning-management-platform-privacy-policy.html",
//                 "terms": "https://za.pearson.com/Pulse-learning-management-platform-terms-of-use.html"
//      }
//      },
//      {
//         "id": "5806a2e26e84cd0058ad8afa",
//              "name": "MyPedia Demo School",
//              "saId": "57fbbd5f418c8e006b8eb5b1",
//              "countryCode": "",
//              "geoId": "574647211a608f1e001ebf12",
//              "geoName": "India",
//              "saName": "MyPedia-SIN",
//              "roleValue": "student",
//              "moodleUserId": 503,
//              "createdBy": "padmin",
//              "externalRef": [],
//         "products": [
//         {
//            "productId": "5880e7b67ea8d24d00796106",
//                 "startDate": "2017-01-19T00:00:00.000Z",
//                 "endDate": "2020-12-31T00:00:00.000Z"
//         }
//      ],
//         "links": {
//         "moodle": "https://www.mypedia.pearson.com/moodle/aaa5fe4d0a6c44a29dd4f83c0ff05555/",
//                 "moodleToken": "d260dcc1ab2441bf80e49e2a6f195e91",
//                 "languagePackAPI": "http://caas.pearson-intl.com/appconfig/s3path?folder=pulse-lms/language_pack&configfile=pulse.mypedia.language_pack.json",
//                 "appConfigAPI": "http://caas.pearson-intl.com/appconfig/s3path?folder=pulse-lms/config&configfile=pulse.mypedia.app.config.json",
//                 "website": "https://mypedia.pearson.com",
//                 "services": "http://api-aws-sin.pulse.pearson.com",
//                 "appConfig": "https://s3.amazonaws.com/pulse-lms/config/pulse.mypedia.app.config.json",
//                 "heroImage": "https://s3.amazonaws.com/pulse-lms/content/school-hero-1.jpg",
//                 "textbooksThumbnail": "https://s3.amazonaws.com/pulse-lms/content/default_textbooks.jpg",
//                 "cookie": "https://s3.amazonaws.com/pulse-lms/legal/cookie.htm",
//                 "privacy": "https://s3.amazonaws.com/pulse-lms/legal/in/privacy.htm",
//                 "terms": "https://s3.amazonaws.com/pulse-lms/legal/in/terms.htm"
//      }
//      }
//  ],
//      "loggedIn": "2017-11-09T16:41:46.164Z"
//   }

}
