package us.ctp.techtask.data.remote.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import ua.at.tsvetkov.application.AppConfig;
import us.ctp.techtask.data.remote.AbstractParser;
import us.ctp.techtask.data.remote.Rest;
import us.ctp.techtask.util.Key;

/**
 * Created by lordtao on 09.11.2017.
 */

public class SignInParser extends AbstractParser {

   @Override
   public void parse(JSONObject jsonObject) throws Exception {
      if (!isErrorDataInResponce(jsonObject)) {
         String accessToken = getString(jsonObject, "access_token");
         if (accessToken.length() > 0) {
            AppConfig.putString(Key.ACCESS_TOKEN, accessToken, AppConfig.SAVE);
            Rest.setAccessToken(accessToken);
            setSuccess(true);
            setMessage("Received Access token");
         } else {
            setSuccess(false);
            setMessage("Access token is empty!");
         }
      }
   }

   @Override
   public void parse(JSONArray jsonArray) throws Exception {

   }

}
