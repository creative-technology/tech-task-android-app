package us.ctp.techtask.data.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.List;

import ua.at.tsvetkov.util.Log;
import us.ctp.techtask.util.Warning;

/**
 * Created by lordtao on 09.11.2017.
 */

public class CourceSection {

    // temporary mock data
    private static final String mock = "[{\"id\":\"5742735088def244002e8a6a\",\"name\":\"Algebra\",\"thumbnail\":\"https://s3.amazonaws.com/pulse-lms/content/course-images/57361936b0f96c57742edd7f.png\",\"schoolId\":\"5742722bca57901e00d6da44\",\"subject\":\"Mathematics\",\"grade\":\"8\",\"createdDate\":\"2016-05-23T03:04:48.000Z\",\"roleValue\":\"student\",\"startDate\":\"2016-01-01T00:00:00.163Z\",\"endDate\":\"2017-12-31T23:59:59.164Z\",\"moodleId\":\"3\",\"moodleDiscussionForumId\":\"7\",\"externalRef\":[],\"cartridgeIds\":[\"583fc96d1757810030d1a8b2\",\"583c0928b2a66600462f6d9a\",\"583c0915b93c9900434284ff\",\"583bdae53bb3770048d6e05b\",\"582c5be7eef60600e21b87e4\"],\"products\":[],\"createdBy\":\"padmin\"},{\"id\":\"5742735388def244002e8a6b\",\"name\":\"Photosynthesis\",\"thumbnail\":\"https://s3.amazonaws.com/pulse-lms/content/course-images/57361937b0f96c57742edd80.png\",\"schoolId\":\"5742722bca57901e00d6da44\",\"subject\":\"Natural Sciences\",\"grade\":\"8\",\"createdDate\":\"2016-05-23T03:04:51.000Z\",\"roleValue\":\"student\",\"startDate\":\"2015-12-31T05:00:00.745Z\",\"endDate\":\"2018-01-01T04:59:59.745Z\",\"moodleId\":\"4\",\"moodleDiscussionForumId\":\"5\",\"externalRef\":[],\"products\":[{\"endDate\":\"2017-11-10T05:00:00.000Z\",\"startDate\":\"2017-11-01T14:40:25.461Z\",\"productId\":\"58896d1944c10c6a0078cadb\"},{\"endDate\":\"2017-11-10T05:00:00.000Z\",\"startDate\":\"2017-11-01T14:41:52.556Z\",\"productId\":\"58896cfb4f6b07700094cec5\"}],\"createdBy\":\"padmin\"},{\"id\":\"5742735688def244002e8a6c\",\"name\":\"Phonics\",\"thumbnail\":\"https://s3.amazonaws.com/pulse-lms/content/course-images/57361937b0f96c57742edd81.png\",\"schoolId\":\"5742722bca57901e00d6da44\",\"subject\":\"English\",\"grade\":\"3\",\"createdDate\":\"2016-05-23T03:04:54.000Z\",\"roleValue\":\"student\",\"startDate\":\"2015-12-31T05:00:00.102Z\",\"endDate\":\"2018-01-02T04:59:59.102Z\",\"moodleId\":\"5\",\"moodleDiscussionForumId\":\"3\",\"externalRef\":[],\"cartridgeIds\":[\"599c19519be58b3000c2dbae\"],\"products\":[{\"endDate\":\"2017-11-10T05:00:00.000Z\",\"startDate\":\"2017-11-01T14:17:03.480Z\",\"productId\":\"58896cfb4f6b07700094cec5\"}],\"createdBy\":\"padmin\"},{\"id\":\"5742735a88def244002e8a6d\",\"name\":\"Comprehension\",\"thumbnail\":\"https://s3.amazonaws.com/pulse-lms/content/course-images/57361938b0f96c57742edd82.png\",\"schoolId\":\"5742722bca57901e00d6da44\",\"subject\":\"Reading\",\"grade\":\"3\",\"createdDate\":\"2016-05-23T03:04:58.000Z\",\"roleValue\":\"student\",\"startDate\":\"2016-01-01T00:00:00.446Z\",\"endDate\":\"2017-12-31T23:59:59.447Z\",\"moodleId\":\"6\",\"moodleDiscussionForumId\":\"2\",\"createdBy\":\"padmin\"},{\"id\":\"583be02634a3d7007830d59d\",\"name\":\"Fractions\",\"thumbnail\":\"https://s3.amazonaws.com/pulse-lms/content/course-images/57361936b0f96c57742edd7f.png\",\"schoolId\":\"5742722bca57901e00d6da44\",\"subject\":\"Mathematics\",\"grade\":\"Higher Ed\",\"createdDate\":\"2016-11-28T07:43:34.263Z\",\"roleValue\":\"student\",\"startDate\":\"2016-01-01T00:00:00.622Z\",\"endDate\":\"2017-12-31T23:59:59.622Z\",\"moodleId\":\"9\",\"externalRef\":[],\"products\":[],\"createdBy\":\"padmin\"}]";
    private static List<CourseItem> courseList = null;

    public static CourceSection createFromJSON(JSONArray jsonArray) {
        CourceSection courceSection = new CourceSection();
        try {
            JSONArray mockArray = new JSONArray(mock);
            courseList = parseJSON(mock);
        } catch (JSONException e) {
            Log.e(e);
        }
        Warning.todo(jsonArray, "Need to parse jsonArray or mockArray for your needs");
        return courceSection;
    }

    public static List<CourseItem> createCoursesFromMock() {
        return parseJSON(mock);
    }

    private static List<CourseItem> parseJSON(String jsonString) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<CourseItem>>(){}.getType();
        List<CourseItem> list = (List<CourseItem>)gson.fromJson(jsonString, listType);

        return list;
    }

    // Example of JSON

//
//   [
//   {
//      "id": "5742735088def244002e8a6a",
//           "name": "Algebra",
//           "thumbnail": "https://s3.amazonaws.com/pulse-lms/content/course-images/57361936b0f96c57742edd7f.png",
//           "schoolId": "5742722bca57901e00d6da44",
//           "subject": "Mathematics",
//           "grade": "8",
//           "createdDate": "2016-05-23T03:04:48.000Z",
//           "roleValue": "student",
//           "startDate": "2016-01-01T00:00:00.163Z",
//           "endDate": "2017-12-31T23:59:59.164Z",
//           "moodleId": "3",
//           "moodleDiscussionForumId": "7",
//           "externalRef": [],
//      "cartridgeIds": [
//      "583fc96d1757810030d1a8b2",
//              "583c0928b2a66600462f6d9a",
//              "583c0915b93c9900434284ff",
//              "583bdae53bb3770048d6e05b",
//              "582c5be7eef60600e21b87e4"
//    ],
//      "products": [],
//      "createdBy": "padmin"
//   },
//   {
//      "id": "5742735388def244002e8a6b",
//           "name": "Photosynthesis",
//           "thumbnail": "https://s3.amazonaws.com/pulse-lms/content/course-images/57361937b0f96c57742edd80.png",
//           "schoolId": "5742722bca57901e00d6da44",
//           "subject": "Natural Sciences",
//           "grade": "8",
//           "createdDate": "2016-05-23T03:04:51.000Z",
//           "roleValue": "student",
//           "startDate": "2015-12-31T05:00:00.745Z",
//           "endDate": "2018-01-01T04:59:59.745Z",
//           "moodleId": "4",
//           "moodleDiscussionForumId": "5",
//           "externalRef": [],
//      "products": [
//      {
//         "endDate": "2017-11-10T05:00:00.000Z",
//              "startDate": "2017-11-01T14:40:25.461Z",
//              "productId": "58896d1944c10c6a0078cadb"
//      },
//      {
//         "endDate": "2017-11-10T05:00:00.000Z",
//              "startDate": "2017-11-01T14:41:52.556Z",
//              "productId": "58896cfb4f6b07700094cec5"
//      }
//    ],
//      "createdBy": "padmin"
//   },
//   {
//      "id": "5742735688def244002e8a6c",
//           "name": "Phonics",
//           "thumbnail": "https://s3.amazonaws.com/pulse-lms/content/course-images/57361937b0f96c57742edd81.png",
//           "schoolId": "5742722bca57901e00d6da44",
//           "subject": "English",
//           "grade": "3",
//           "createdDate": "2016-05-23T03:04:54.000Z",
//           "roleValue": "student",
//           "startDate": "2015-12-31T05:00:00.102Z",
//           "endDate": "2018-01-02T04:59:59.102Z",
//           "moodleId": "5",
//           "moodleDiscussionForumId": "3",
//           "externalRef": [],
//      "cartridgeIds": [
//      "599c19519be58b3000c2dbae"
//    ],
//      "products": [
//      {
//         "endDate": "2017-11-10T05:00:00.000Z",
//              "startDate": "2017-11-01T14:17:03.480Z",
//              "productId": "58896cfb4f6b07700094cec5"
//      }
//    ],
//      "createdBy": "padmin"
//   },
//   {
//      "id": "5742735a88def244002e8a6d",
//           "name": "Comprehension",
//           "thumbnail": "https://s3.amazonaws.com/pulse-lms/content/course-images/57361938b0f96c57742edd82.png",
//           "schoolId": "5742722bca57901e00d6da44",
//           "subject": "Reading",
//           "grade": "3",
//           "createdDate": "2016-05-23T03:04:58.000Z",
//           "roleValue": "student",
//           "startDate": "2016-01-01T00:00:00.446Z",
//           "endDate": "2017-12-31T23:59:59.447Z",
//           "moodleId": "6",
//           "moodleDiscussionForumId": "2",
//           "createdBy": "padmin"
//   },
//   {
//      "id": "583be02634a3d7007830d59d",
//           "name": "Fractions",
//           "thumbnail": "https://s3.amazonaws.com/pulse-lms/content/course-images/57361936b0f96c57742edd7f.png",
//           "schoolId": "5742722bca57901e00d6da44",
//           "subject": "Mathematics",
//           "grade": "Higher Ed",
//           "createdDate": "2016-11-28T07:43:34.263Z",
//           "roleValue": "student",
//           "startDate": "2016-01-01T00:00:00.622Z",
//           "endDate": "2017-12-31T23:59:59.622Z",
//           "moodleId": "9",
//           "externalRef": [],
//      "products": [],
//      "createdBy": "padmin"
//   }
//]

}
