package us.ctp.techtask.data.remote;

import android.text.TextUtils;

import org.json.JSONObject;

import java.net.HttpURLConnection;

import ua.at.tsvetkov.data_processor.templates.JSONDataAbstract;

/**
 * Created by lordtao on 09.11.2017.
 */

public abstract class AbstractParser extends JSONDataAbstract {

   /**
    * Checks the JSONObject for error data in the responce.
    * Creates and puts error string in to the "message" field if object contain an error data.
    *
    * @param jsonObject
    * @return false - object contain an error data (statusCode, error, message). true - object contain correct JSON data
    */
   protected boolean isErrorDataInResponce(JSONObject jsonObject) {
      int statusCode = jsonObject.optInt("statusCode");
      String error = jsonObject.optString("error");
      String message = jsonObject.optString("message");
      if (!TextUtils.isEmpty(error) && !TextUtils.isEmpty(message) ) {
         setMessage("Status code " + statusCode + ".\nError: " + error + "\nMessage: " + message);
         return true;
      } else {
         return false;
      }
   }

}
