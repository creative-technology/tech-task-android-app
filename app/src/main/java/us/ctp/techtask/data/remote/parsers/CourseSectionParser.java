package us.ctp.techtask.data.remote.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import ua.at.tsvetkov.util.Log;
import us.ctp.techtask.data.models.CourceSection;
import us.ctp.techtask.data.remote.AbstractParser;

/**
 * Created by lordtao on 09.11.2017.
 */

public class CourseSectionParser extends AbstractParser {

   @Override
   public void parse(JSONObject jsonObject) throws Exception {
       if (isErrorDataInResponce(jsonObject)) {
           Log.e("Only JSONArray must be");
      }
   }

   @Override
   public void parse(JSONArray jsonArray) throws Exception {
       CourceSection.createFromJSON(jsonArray);
   }

}
