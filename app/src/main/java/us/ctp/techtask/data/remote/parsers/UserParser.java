package us.ctp.techtask.data.remote.parsers;

import org.json.JSONArray;
import org.json.JSONObject;

import us.ctp.techtask.data.models.User;
import us.ctp.techtask.data.remote.AbstractParser;

/**
 * Created by lordtao on 09.11.2017.
 */

public class UserParser extends AbstractParser {

   public User user;

   @Override
   public void parse(JSONObject jsonObject) throws Exception {
      if (!isErrorDataInResponce(jsonObject)) {
         user = User.createFromJSON(jsonObject);
         setSuccess(true);
         setMessage("Received User info");
      }
   }

   @Override
   public void parse(JSONArray jsonArray) throws Exception {

   }

}
