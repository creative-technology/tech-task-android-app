package us.ctp.techtask.data.models;

/**
 * Created by Angel on 10.11.2017.
 */

public class CourseItem {

    private String id;
    private String subject;
    private String name;
    private String thumbnail;

    public CourseItem(String id, String subject, String name, String thumbnail) {
        this.id = id;
        this.subject = subject;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public String getSubject() {
        return subject;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
